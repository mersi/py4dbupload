#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 09-Oct-2020

# This script masters the registration of the OT Sensor components.

import os,traceback,sys,time

from AnsiColor   import Fore, Back, Style
from DataReader  import TableReader,scale,scale2
from Sensor      import *
from Utils       import *
from optparse    import OptionParser
from decimal     import Decimal, ROUND_HALF_UP
from progressbar import *


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the excel files with measurement data.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)>2:
      p.error("accepts at most one argument!")


   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug

   # write the description and the attributes for all the Wafer components
   wafer_conf = {
      'description'      : 'PS-s Wafer from Pre Production.',
      'attributes'       : [('Sensor Type','PreProduction'),('Status','Good')],
   }

   # write the description and the attributes for all the Sensor components
   sensor_conf = {
      'description'      : 'PS-s Sensor from Pre Production.',
      'attributes'       : [('Sensor Type','PreProduction'),('Status','Good')],
   }

   # write the version and the description for the IV data
   iv_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test',
             }

   # write the version and the description for the CV data
   cv_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_2',
             }

   # write the version and the description for the bad strip
   bad_strip_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_3',
                    }

   # write the version and the description for the summary data
   summary_data = {
      'data_version'     : 'v1',
      'data_comment'     : 'test_4',
                  }

   # gather excel files conatining the data measurements
   data_files = sorted( search_files(opt.data_path,'*.txt') )

   wafers     = UploaderContainer('Wafers')
   sensors    = UploaderContainer('Sensors')
   conditions = UploaderContainer('ConditionData')

   envelope_ids = None
   if len(args)!=0:  envelope_ids = TableReader(args[0])

   runp = RunProvider(use_database=False)  # set to True for extracting run from
                                           # CMSR Database
   runp.setDbQuery('select r.run_number from trker_cmsr.trk_ot_test_nextrun_v r')
   #runp.setDbQuery('select r.run_number from trker_int2r.trk_ot_test_nextrun_v r')

   nFiles = len(data_files)
   bar = progressbar(range(nFiles), widgets=['Processing Hamamatsu files: ', \
                Bar('=', '[', ']'), ' ', Percentage()])

   for i in bar:
      f = data_files[i]
      sd = TableReader(f, d_offset=1, m_rows=20,tabSize=43,csv_delimiter="\t")
      iv = TableReader(f, d_offset=22,m_rows=51,tabSize=15,csv_delimiter="\t")
      cv = TableReader(f, d_offset=75,m_rows=41,tabSize=15,csv_delimiter="\t")

      # store wafer classes for registration
      wafer = OTwafer(wafer_conf,sd,envelope_ids)
      wafers.add(wafer)

      # store sensor classes for registration
      sensor = OTsensor(sensor_conf,sd,envelope_ids)
      sensors.add(sensor)

      # set run number and run comment
      run_data = {
         'run_type'         : 'VQC',
         'run_number'       : runp.run_number(),
         'run_comment'      : 'test for development',
                }

      IV = OTSensorMeasurements(run_data,iv_data,OTDataType.iv,iv,sd)
      CV = OTSensorMeasurements(run_data,cv_data,OTDataType.cv,cv,sd)
      BS = OTSensorMeasurements(run_data,bad_strip_data,OTDataType.bad_strip,cv,sd)
      SMMRY = OTSensorMeasurements(run_data,summary_data,OTDataType.summary,cv,sd)
      #SMMRY.attach_part = False
      SMMRY.add_child( IV )
      SMMRY.add_child( CV )
      SMMRY.add_child( BS )

      conditions.add(SMMRY)
      time.sleep(0.1)

   streams.flush()
   wafers.dump_xml_data()
   sensors.dump_xml_data(wafers.uploaders)
   conditions.dump_xml_zip()
