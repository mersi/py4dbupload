#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 23-Apr-2022

# This script masters the part update. It is normally invoked with the name of 
# a csv file that contains the ID of the component to be modified. The csv file 
# must contains one column only with an header name that specify the type of ID:
# 'serial' or 'barcode' or 'name_label'. The ID of components can also be inserted
# via the --id option thus avoiding to create the csv file; in such a case the 
# type of ID must be specified with the --idtype option.

import os,sys,fnmatch
import pandas as pd

from AnsiColor  import Fore, Back, Style
from Exceptions import *
from UpdatePart import Update, BaseUploader
from Utils      import DBaccess, DBupload, IdType, UploaderContainer
from optparse   import OptionParser


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] [csv file name]", version="1.1")

   p.add_option( '--id',
                  action  = 'append',
                  type    = 'string',
                  default = [],
                  dest    = 'ids',
                  metavar = 'STR',
                  help    = 'ID of the components to be modified')

   p.add_option( '--idtype',
                  type    = 'string',
                  default = 'serial_number',
                  dest    = 'idtype',
                  metavar = 'STR',
                  help    = 'Type of ID: \'serial\', \'barcode\', \'name_label\'')
   
   p.add_option( '-i','--inserter',
               type    = 'string',
               default = None,
               dest    = 'inserter',
               metavar = 'STR',
               help    = 'Override the account name of the operator that performs the data update.')
   
   p.add_option( '--dev',
                  action  = 'store_true',
                  default = False,
                  dest    = 'isDevelopment',
                  help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
                  action  = 'store_true',
                  default = False,
                  dest    = 'upload',
                  help    = 'Perform the data upload in database')
   
   p.add_option( '--verbose',
                  action  = 'store_true',
                  default = False,
                  dest    = 'verbose',
                  help    = 'Force the uploaders to print their configuration and data')

   p.add_option( '--debug',
                  action  = 'store_true',
                  default = False,
                  dest    = 'debug',
                  help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
 
   p.add_option( '--expert',
                  action  = 'store_true',
                  default = False,
                  dest    = 'expert',
                  help    = 'Set the Expert mode for the component update.')


   (opt, args) = p.parse_args()

   if len(args)>1:
      p.error('too many arguments!')

   if len(opt.ids)==0 and len(args)==0:
      p.error('at least one serial number must be entered!')

   # Load type of ID and IDs from csv file
   if len(args)==1:
      df = pd.read_csv(args[0])
      opt.idtype = df.columns[0]
      opt.ids    = df[df.columns[0]].tolist()   

   # Check the type of ID
   try:
      IdType[str(opt.idtype)]  # check if type ID is recognized
   except:
      types = [el.name for el in IdType]
      print(Fore.RED+f'{opt.idtype}'+Style.RESET_ALL+' is not a valid ID type!')
      print('valid ID type are: '+Fore.BLUE+f'{types}'+Style.RESET_ALL)
      exit(1)
  
   if opt.verbose:
      print (f'Type of id: {opt.idtype}')
      print (f'To be modified: \n {opt.ids}')
      BaseUploader.verbose = True


   # Getting the part data from database
   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   #db = DBaccess(database=f'trker_{database}', verbose=opt.debug)
   
   data = UploaderContainer('update',opt.inserter)
   mode = 'expert' if opt.expert else 'basic'
   for id in opt.ids:
      part = Update(id,opt.idtype,mode=mode,debug=opt.debug)
      part.steer_update()
      data.add(part)
      
   data.dump_xml_data()
   
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db = DBupload(database=BaseUploader.database,path_to_dbloader_api=path ,verbose=opt.debug)
   if opt.upload:
      # The test mode of the DB-Loader does not work for attributes 
      db.upload_data('update.xml',not opt.upload)

