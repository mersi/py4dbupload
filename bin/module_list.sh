#!/bin/bash

DB='trker_cmsr'

function getPartsOfKind() {
  KIND="$1"
  python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --krb "select p.BARCODE from ${DB}.parts p where p.kind_of_part like '${KIND}' and p.barcode is not null order by p.barcode" --all -n | gawk 'BEGIN {doPrint=0} // {if (doPrint) print} /^BARCODE/ {doPrint=1}'
  # python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login "select p.kind_of_part as type, p.BARCODE, nvl(l.location_name, 'IN A PACKAGE') as location, p.id from ${DB}.parts p left join  ${DB}.trkr_locations_v l on  l.location_id=p.location_id where p.kind_of_part = '${KIND}' and p.barcode is not null order by l.location_name asc, p.barcode" --all -n | column -s, -t
  # python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login "select * from ${DB}.parts p where p.kind_of_part = '${KIND}' " --all -n | column -s, -t
}

getPartsOfKind "_S Module" | egrep . 


