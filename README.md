This package contains python classes that produce the XML files needed for the data
upload in the database. The classes read the input data from various formats (excel,
csv) and integrate them with additional information necessary for the process of 
insertion into the database. This additional infos are provided to the python code 
in the form of configuration parameters; the code checks that they are consistent 
with the data type being inserted in the database and, if so, produces the XML file.
The code is written to work on python3.

**Cloning the package**

As the package contains two submodules the options --recursive must be used while cloning the package, i.e.

git clone --recursive https://gitlab.cern.ch/cms-ph2-database/py4dbupload



Structure of the package
========================
    README.md  .... this file
    bin  .......... contains the script for setting up the environment
    data  ......... contains the data file for Sensors, CBC, RD53 chips
    modules  ...... contains the python modules  
    test  ......... contains the applications to produce the XML files


Dependencies & Intallation
==========================
The package modules have been coded to work under python3 which is therefore 
required to be installed in the working machine. The modules also depends on 
other python software that have to be installed on the machine as well. It is
reccommended to perform this operation through the pip3 command. Instructions
about how to install the pip3 comand in the working machine follows:

 + on mac: https://www.delftstack.com/it/howto/python/python-install-pip3-mac/
 + on linux ubuntu, mint:  sudo apt install python3-pip
 + on lunix centos:        sudo yum –y install python3-pip

The packages required are the following

 + **lxml**
 + **openpyxl**
 + **xlrd**
 + **progressbar2**
 + **pandas**

 you can install them all with `sudo pip3 install -r requirements.txt`

**CAVEAT on MAC arm64 (OS Monterey)**: lxml depends on libxml2 and libxslt. These
libraries may be already installed in the system but not updated for the lxlm
package. As a consequence error arises when the etree gets imported. To fix 
this is it necessary to install the latest version of the libxml2 and libxslt 
in the machine through Homebrew. Proceeds as follows:

1)  Unistall any previous version of Homebrew
    https://github.com/Homebrew/install/blob/master/README.md

2)  Install the new version of homebrew
    https://github.com/Homebrew/install/blob/master/README.md

3)  Install the updated version of libxml2 and libxslt:
    brew install libxml2
    brew install libxslt

4)  Exploit the updated version of the libxml2 and libxslt for compiling software by adding the following lines to the .bash_profile:
    `export LDFLAGS="-L/opt/homebrew/opt/libxml2/lib -L/opt/homebrew/opt/libxslt/lib"`
    `export CPPFLAGS="-I/opt/homebrew/opt/libxml2/include -I/opt/homebrew/opt/libxslt/include"`
    here we assume that Homebrew installed the libraries under /opt/homebrew/opt

5)  reisntalling the lxml package by forcing the rebuild of the code in order to pick up the updated version of libxml2 and libxslt
    sudo --preserve-env pip3 install lxml --force-reinstall --no-cache-dir --global-option=build_ext



The package also uses the **rhapi.py** client to check the database before generating
the XML file. This package needs the following libraries:

+ **requests** ....... install with   `sudo pip3 install requests`
+ **ilock** .......... install with   `sudo pip3 install ilock`


Testing the code
================
The code can be tested to produce the file for uploading some components. Each of 
the supported component type have an application under the test directory that can
be run under python3. These applications read from data files the description and 
the measurement of the component to be uploaded for producing the XML file.

In Order to run the code the modules directory must be added to the PYTHONPATH
environment varialble. This can be done by sourcing the setup scripts unber bin:

+  source bin/setup.sh


Here are the list of the available applications:

+ Sensors and Wafers:  enterSensorPreSeries.py
              usage:  python3 test/enterSensorPreSeries.py data/Sensors/PreSeries/PSsPreSeries.txt -d data/Sensors/PreSeries/

+ RD53 data: enterRD53Data.py
              usage: python3 test/enterRD53Data.py data/RD53/

+ CBC chips and wafers:  enterCBCdata.py
              usage:  python3 test/enterCBCdata.py data/CBC_chips/

+ Update components: enterUpdate.py
              usage: python3 test/enterUpdate.py -s <serial_n> ....

+ FrontEnd Hybrids: enter8CBC3Hybrids.py
              usage: python3 test/enter8CBC3Hybrids.py data/8CBC3/inventory -d data/8CBC3/feh_test/


Scripts for database uopload
============================
The production script for registering components and data in the database are
put in the **run** directory.

+ registerOTSensor.py: register the OT Sensor and Wafer components reading the 
data from Hamamatsu txt file.
```
  Usage: registerOTSensors.py [options] 

       Options: 
         --version           show program's version number and exit.
         -h, --help          show this help message and exit.
         -d STR, --data=STR  Path to the excel files with measurement data.
         --verbose           Force the uploaders to print their configuration and data

  Test (from run directory): python3 registerOTSensors.py --data ../data/Sensors/VPX34342/PSs_L
````

+ registerHM.py: register the Halfmoon and Wafer components reading the barcodes from an ascii file.

```
 Usage: registerHM.py [options] <serial number file>, ...

       Options:
         --version                show program's version number and exit
         -h, --help               show this help message and exit
         -d STR, --data=STR       Path to the excel files with measurement data.
         --date=STR               Date of the component registration
         -o STR, --operator=STR   The operator that performed the QC measurement
         --verbose                Force the uploaders to print their configuration and data
```

+ registerHMData.py: register the Halfmoon data reading the measurement files files.
```
  Usage: registerHMData.py [options] <halfmoon id>

       Options:
         --version               show program's version number and exit
         -h, --help              show this help message and exit
         -d STR, --data=STR      Path to the txt files with measurement data.
         -l STR, --location=STR  Specify where the measurement run have been made.
         -o STR, --operator=STR  Specify the operator that made the measurements.
         --date=STR              Specify the date (format: YYYY-MM-DD) of the measurements.
         --verbose               Force the uploaders to print their configuration and data.

  Test (from run directory): python3 registerHMData.py --data ../data/Halfmoons 41739_046_2-S_HM_NN
```

+ registerCORC.py: register the CROC components reading the cvs table files.
```
 Usage: registerCROC.py [options] [cvs table file]

       Options:
        --version             show program's version number and exit
        -h, --help            show this help message and exit
        -d STR, --data=STR    Path to the csv tables with CROC wafer data.
        --date=STR            Date of the component productioni; overwitten by the
                              date in the cvs table.
        -v STR, --ver=STR     Version type of the components; overwitten by the tag
                              in the csv table.
        -o STR, --operator=STR
                              The operator that performed the data registration
        --no_chip             Do not produce chip data.
        --test                Output only the first wafer for test.
        --verbose             Force the uploaders to print their configuration and
                              data

  Test (from run directory): python3 registerCROC.py --data ../data/CROC/Real/
```

+ registerCBC.py: register the CBC wafer and chips. Requires the test result table, the sinf file and the PDF map of the wafer.
```
Usage: registerCBC.py [options] <path to CBC data>, ...

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -v STR, --ver=STR     Version type of the components; it is superseed by the
                        version in the sinf file.
  -d STR, --desc=STR    Input a description for the data to be uploaded. The
                        description of the chips is superseed with the TYPE
                        and the LOT from the sinf file.
  -i STR, --inserter=STR
                        Overwirtes the account name put in the
                        RECORD_INSERTION_USER column.
  --date=STR            Date of the component production.
  --dev                 Set the development database as target.
  --upload              Perform the data upload in database.
  --verbose             Force the uploaders to print their configuration and
                        data.
  --debug               Force the verbose options in the network query
                        uploaders to print their configuration and data.

  Test (from run directory): python3 registerCBC.py ../data/CBC_chips/V3DAQXH/
```
