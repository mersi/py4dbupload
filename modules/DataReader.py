#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 10-Apr-2019

# This module contains the class for reading data stored into various formats.
# Currently it supprots EXCEL and CSV formats. For accessing the excel data
# sheets it requires the openpyxl package.
#   openpyxl: https://bitbucket.org/openpyxl/openpyxl

import sys, csv
from openpyxl   import load_workbook
from xlrd       import *
from AnsiColor  import Fore, Back, Style
from Exceptions import *

# Ancillary functions #########################################################
def sort_by_column(item):
    return item[0]

def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i:i+n]

def strData(v):
   try:
      v = float(v)
      if v.is_integer():  v = int(v)
   except:
      pass
   data = str(v)
   return data.strip()

def scale(data,key,scale_factor):
   """Scale data arranged as dictionary of vectors."""
   from decimal import Decimal, ROUND_HALF_UP
   try:
      to_be_scaled = data[key]
      for pos,d in enumerate(to_be_scaled):
         scaled   = str(float(d)*scale_factor)
         rounding = Decimal(scaled).quantize(Decimal('0.001'),ROUND_HALF_UP)
         to_be_scaled[pos] = '{}'.format(rounding)
   except:  pass

def scale2(data,key,scale_factor):
   """"Scale data arranged as vector of dictionaries."""
   from decimal import Decimal, ROUND_HALF_UP
   try:
      for d in data:
         scaled   = str(float(d[key])*scale_factor)
         rounding = Decimal(scaled).quantize(Decimal('0.001'),ROUND_HALF_UP)
         d[key] = '{}'.format(rounding)
   except:  pass

def consistency_check(data,*keys):
   """Check if all data under the header specified in keys are different."""
   sequences = []
   for row in data:
      values = []
      for k in keys:
         values.append(row.get(k))
         sequences.append( all(element==values[0] for element in values) )
   return True if not all(sequences) else False
#   if int(sys.version[0])<3:
#      if isinstance(string,basestring): string = string.strip()
#   else:
#      if isinstance(string,str): string = string.strip()

def getColumnData(data,expr):
    import re
    for k in data.keys():
        if re.compile(expr).match(k):
            return data[k]

def checkForHexValue(data,expr):
    import re
    for k in data.keys():
        if re.compile(expr).match(k):
            if "hex" in k: return True
    return False

# Reader class ################################################################
class TableReader():
   """Class to gather the table data from a worksheet."""

   def __init__(self, filename, sheetname='', t_offset=0, d_offset=0, m_rows=0,\
                csv_delimiter=';',tabNumber=8,tabSize=8):
      """Constructor: it requires the input filename (e.g. the path + the name
         of the file), the name of the excel datasheet (sheetname), the number
         of rows to be skip before reading the table (t_offset), the number of
         rows to be skip after the first table row (d_offset), the number of
         rows to read from the file (m_rows) and the delimiter used in the csv
         file formati(csv_delimiiter). If m_rows is omitted it reads up to the
         last non empty values in the first column. The filename extension sets
         the file format. Currently two format are recognized: "xlsx" for excel
         and "csv" for the CSV format."""

      self.filename  = filename
      self.sheetname = sheetname
      self.tabN      = tabNumber
      self.tabS      = tabSize
      self.columns   = 0
      self.rows      = 0

      import os.path
      extension = os.path.splitext(filename)[1]
      exists    = os.path.isfile(filename)
      if not exists:
         raise FileDoesNotExist(filename)

      # excel data processing
      if extension == '.xls':
         wb = open_workbook(filename)
         if sheetname in wb.sheet_names(): datasheet = wb.sheet_by_name(sheetname)
         else:                             datasheet = wb.sheet_by_index(0)
         self.load_with_xlrd(datasheet,t_offset,d_offset,m_rows)

      if extension == '.xlsx':
         wb = load_workbook(filename)
         if sheetname in wb.sheetnames: datasheet = wb[sheetname]
         else:                          datasheet = wb.active
         self.load_with_openpyxl(datasheet,t_offset,d_offset,m_rows)

      # csv data processing
      if extension == '.csv' or extension == '.txt' or extension == '.sinf':
         #newline='', encoding='utf-8', errors='ignore'
         with open(filename,newline='\n', encoding='utf-8', errors='ignore') as csvfile: #'rb') as csvfile:
            datasheet = csv.reader((line.replace('\0','') for line in csvfile) , delimiter=csv_delimiter)
            rows = sum(1 for row in csvfile) if m_rows==0 else m_rows
            csvfile.seek(0)
            self.load_csv_data(datasheet, rows, d_offset)
   
   
   def load_with_xlrd(self, datasheet, t_offset=0, d_offset=0, m_rows=0):
      """Load data from excel file."""
      max_rows = datasheet.nrows if m_rows==0 else min(m_rows,datasheet.nrows)

      #Find out the header row and data
      header_row  = 0
      data = []
      for header_row in range(t_offset, max_rows+1):
         if datasheet.cell_type(rowx=header_row,colx=1)!=XL_CELL_EMPTY: break
      max_cols=0
      try:
         while datasheet.cell_type(rowx=header_row, colx=max_cols)!=XL_CELL_EMPTY:
            value = datasheet.cell_value(rowx=header_row, colx=max_cols)
            data.append( (0, max_cols, strData(value)) )
            max_cols+=1
      except:
         if datasheet.ncols != max_cols:
            raise ColumnInconsistency(datasheet, header_row, max_cols)

      data_start_row = header_row + d_offset + 1
      for row in range(data_start_row, max_rows):
         #stop at the empty cell on the first column if m_rows==0
         if datasheet.cell_type(rowx=row,colx=0)==XL_CELL_EMPTY and m_rows==0:
            break
         for col in range(datasheet.ncols):
            value = datasheet.cell_value(rowx=row,colx=col)
            data.append( (row-data_start_row+1, col, strData(value)) )

      # load data into matrix
      self.rows    = max(i[0] for i in data) + 1
      self.columns = max(i[1] for i in data) + 1
      self.data = [['' for c in range(self.columns)] for r in range(self.rows)]
      for item in data:
         r = item[0]
         c = item[1]
         if self.data[r][c] != '':
             raise MatrixCellAlreadyFilled(self.__class__.__name__,r,c,\
                                           self.data[r][c])
         self.data[r][c] = item[2]


   def load_with_openpyxl(self, datasheet, t_offset=0, d_offset=0, m_rows=0):
      """Load data from xlsl excel file."""
      max_rows = datasheet.max_row if m_rows==0 else min(m_rows,datasheet.max_row)

      #Find out the header row and data
      header_row=0
      data = []
      for header_row in range(max(t_offset,1), max_rows+1):
         if datasheet.cell(row=header_row,column=1).value!=None: break
      max_cols=1
      try:
         while datasheet.cell(row=header_row, column=max_cols).value!=None:
            value = datasheet.cell(row=header_row, column=max_cols).value
            data.append( (0, max_cols, strData(value)) )
            max_cols+=1
      except:
         if datasheet.max_column != max_cols:
            raise ColumnInconsistency(datasheet, header_row, max_cols)

      data_start_row = header_row + d_offset + 1
      for row in range(data_start_row, max_rows+1):
         #stop at the empty cell on the first column if m_rows==0
         if datasheet.cell(row=row,column=1).value==None and m_rows==0:
            break
         for col in range(1,datasheet.max_column+1):
            value = datasheet.cell(row=row,column=col).value
            data.append( (row-data_start_row+1, col, strData(value)) )

      # load data into matrix
      self.rows    = max(i[0] for i in data) + 1
      self.columns = max(i[1] for i in data) + 1
      self.data = [['' for c in range(self.columns)] for r in range(self.rows)]
      for item in data:
         r = item[0]
         c = item[1]
         if self.data[r][c] != '':
             raise MatrixCellAlreadyFilled(self.__class__.__name__,r,c,\
                                           self.data[r][c])
         self.data[r][c] = item[2]


   def load_csv_data(self, datasheet, rows, d_offset=0):
      """Load data from csv file."""

      data = []
      row    = 0
      column = 0
      for line,row_data in enumerate(datasheet):
         column = 0
         if line >= d_offset and row <= rows:
            for item in row_data:
               data.append( (row,column,item.strip()) )
               column += 1
            row += 1
      #load into matrix
      self.rows    = max(i[0] for i in data) + 1
      self.columns = max(i[1] for i in data) + 1
      self.data = [['' for c in range(self.columns)] for r in range(self.rows)]
      for item in data:
         r = item[0]
         c = item[1]
         if self.data[r][c] != '':
             raise MatrixCellAlreadyFilled(self.__class__.__name__,r,c,\
                                           self.data[r][c])
         self.data[r][c] = (item[2]).encode("ascii", "ignore").decode()

   def getHeaderData(self):
      """Returns a vector of the header values, sorted with column order."""
      return self.data[0]

   def getDataAsCWiseDict(self):
      """Returns a column wise dictionary of the data. The dictionary keys are
         the header data. The dictionary values are vectors of row data in the
         column (row wise ordered)."""
      data = {}
      for column,el in enumerate(self.data[0]):
         data[el] = []
         for row in range(1,self.rows):
            data[el].append( self.data[row][column] )
      return data

   def getDataAsCWiseDictRowSplit(self):
      """Returns a column wise dictionary of the data splitted row wise. The
         dictionary keys are the header data. The dictionary values are vectors
         of row data in the column."""
      data = []
      for row in range(1,self.rows):
         row_data = {}
         for column,el in enumerate(self.data[0]):
            row_data[el] = self.data[row][column]
         data.append( row_data )
      return data

   def getDataAsCWiseVector(self):
      """Returns a vector of tuples, column wise ordered, each having the data
         of the column. The tuples values are ordered according to the rows."""
      data = []
      for column in range(self.columns):
         data.append( tuple(self.data[r][column] for r in range(self.rows))  )
      return data

   def getDataAsRWiseVector(self):
      """Returns a vector of tuples, row wise ordered, each having the data
         of the row. The tuples values are ordered according to the columns."""
      data = []
      for row in range(self.rows):
         data.append( tuple(self.data[row][c] for c in range(self.columns))  )
      return data

   def getDataFromTag(self,tag):
      """Returns the data associated to a string tag from a two column table."""
      if self.columns==2:
         data = self.getDataAsRWiseVector()
         for item in data:
            if item[0]==tag:  return item[1]
      return None

   def __str__(self):
      """Print out the data content."""
      from decimal import Decimal, ROUND_HALF_UP
      txt = []

      #extract the first column header
      header_data = self.getHeaderData()
      fc_hdata    = header_data[0]
      oc_hdata    = header_data[1:]
      data = self.getDataAsCWiseDict()

      table = list(chunks(oc_hdata, self.tabN-1))
      for t in table:
         txt.append( '\n  ' + Back.YELLOW + Fore.RED + Style.BOLD + ('{:<%d} '%self.tabS).format(fc_hdata) )
         [ txt.append( ('{:<%d} '%self.tabS).format(element) ) for element in t]
         txt.append( Style.RESET_ALL + '\n' )

         for p, point in enumerate(data[fc_hdata],0):
            color_bck = Back.CYAN if p%2 else Back.WHITE
            txt.append( '  ' )
            txt.append( Fore.BLUE + color_bck + ('{:<%d} '%self.tabS).format(point) + Fore.BLACK)
            for element in t:
               y_data = data[element][p]
               #try:
               #   float(y_data)
               #   y_data = Decimal(y_data).quantize(0,ROUND_HALF_UP)
               #except: pass
               txt.append( ('{:<%d} '%self.tabS).format(y_data) )
            txt.append( Style.RESET_ALL + '\n' )
      return ''.join(txt)
