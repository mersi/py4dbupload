#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 22-March-2019

# This module contains general purpose code.

import os,fnmatch,sys,time
#from signal import raise_signal
from Exceptions  import BadQuery,RecoveryNotAvailable, DBAPINotAvailable,\
                        TooManyMatchesInFilelist, UploadFailure
from enum        import Enum
from lxml        import etree
from progressbar import *

IdType = Enum('IdType','serial_number barcode name_label')


def clear():
   # for windows
   if os.name == 'nt':
      _ = os.system('cls')

   # for mac and linux
   else:
      _ = os.system('clear')




def search_files(top_level_path='.', pattern='',remove_temporary=False):
    """Search recursively from the input top level path for files matching the pattern
       given as input parameter. It returns the list of found files."""
    found = []
    for dirpath, dirnames, files in os.walk(top_level_path):
        for name in files:
            accept = (name[0]!='~' or remove_temporary==False)
            if fnmatch.fnmatch(name,pattern) and accept:
                found.append( os.path.join(dirpath, name) )
    return found

def join_data_files(*args):
   """Match the basename of files from several lists; for each match returns
      the tuple of matching files inside a dictionary whose key is the file
      basename."""
   if len(args)==0:  return None
   result = {}
   basename = [ os.path.splitext(os.path.basename(f))[0] for f in args[0] ]
   same_base = set(basename)
   for arg in args[1:]:
      nb = [ os.path.splitext(os.path.basename(f))[0] for f in arg ]
      same_base = same_base.intersection(nb)
   for it in same_base:
      files = []
      for arg in args:
         matches = [f for f in arg if fnmatch.fnmatch(f,'*%s*'%it)]
         if len(matches)>1: raise TooManyMatchesInFilelist(1,matches,it)
         files.append(matches[0])
      result[it] = tuple(files)
   return result

def merge_data(*args):
   """Merge the data of the uploader objects given in input and in a new 
      uploader object that is returned. Assumes the data are all located 
      in the DBvar_vs_TxtHeader configuration; data are merged only if 
      keys in the DBvar_vs_TxtHeader are congruent."""
      
   from copy import deepcopy
   
   data = []
   for upl in args:
      try:
         upl_data = upl.retrieve('DBvar_vs_TxtHeader')
         if upl_data == None: raise ValueError('')
      except:
         print('merge_data: cannot retrieve the DBvar_vs_TxtHeader configuration!')
         return None

      if len(data)==0: 
         data.append(deepcopy(upl_data))
      else:
         if data[0].keys() == upl_data.keys():
            data.append(deepcopy(upl_data))
      
   upl = deepcopy(args[0])
   upl.configuration['DBvar_vs_TxtHeader'] = data
   return upl



class RunProvider:
   """Service class providing run numbers for condition data."""

   def __init__(self, default_run=1, increment_run=True, use_database=True,
                database='cmsr', sequencer='trk_ot_test_nextrun_v'):
      """Constructor: it requires the default run number to be used (default_run),
         the steering flag to choose iof the run numbers have to be increased at
         each extraction and the steering flag to choose if the run numbers have
         to be extracted from the dataabse (use_database). It aslo requires the
         database to the used (production if the defualt) and the sequencer for
         increasing the run number.

         Putting use_database to True superseed the default_run setting because the
         database always gives the next available run number.
      """

      self.current_run   = None
      self.default_run   = default_run
      self.increment_run = increment_run
      self.use_database  = use_database
      self.dbQuery = f'select r.run_number from trker_{database}.{sequencer} r'


   def setDbQuery(self, query):
      """Set the database query for ectracting the run numbers."""
      self.dbQuery = query

   def get_run_from_db(self,verbose):
      """Get the run number from the database."""
      try:
         from rhapi import RhApi
      except:
         raise DBAPINotAvailable(self.__class__.__name__,f"RhApi not found!")
         #print( 'Cannot import rhapi: resthub package not in the path.' )
         #print( 'Download the resthub package and run bin/setup.sh' )
         #sys.exit(1)

      dburl = 'https://cmsdca.cern.ch/trk_rhapi'
      DBapi = RhApi(dburl, debug=verbose, sso='login')
      if self.dbQuery=='':  raise BadQuery(self.__class__.__name__, self.dbQuery)
      DBapi.clean(DBapi.qid( self.dbQuery) )
      Qres  = DBapi.json2( self.dbQuery )
      return Qres['data'][0]['runNumber']

   def run_number(self, verbose=False):
      """Return the next run number."""
      run = None
      if self.use_database:
         if self.increment_run:
            self.current_run = self.get_run_from_db(verbose)
            run = self.current_run
         else:
            if self.current_run==None:
               self.current_run = self.get_run_from_db(verbose)
            run = self.current_run
      else:
         if self.current_run==None: self.current_run = self.default_run
         run = self.current_run
         if self.increment_run: self.current_run+=1

      if verbose: print( 'RunProvider: returning run number {}'.format(run) )
      return str(run)


class DBupload:
   """Service class for uploading into the database."""

   def __init__(self, database='cmsr', path_to_dbloader_api='',verbose=False):
      """Constructor: it requires the database where to upload the XML data. By
         default it points to the production database. The development database
         can be accessed using 'int2r'. The path to the db_loader client api is
         also needed.
      """
      self.database = database
      self.dbloader = os.path.join(path_to_dbloader_api,'cmsdbldr_client.py')
      self.verbose  = verbose

      if os.path.exists(self.dbloader)==False:
            raise DBAPINotAvailable(self.__class__.__name__,f"command {self.dbloader} not found!")

   def upload_data(self, filename, isTest=False):
      """Performs the XML upload through the netwrok with the cmsdbloader client. 
         Requires the filename to be upload (to be found in the current directory).
      """
      import subprocess
      import os,sys

      #if recovery_path!=None:
      #   if os.path.exists(recovery_path)==False:
      #      raise RecoveryNotAvailable(self.__class__.__name__,f"{recovery_path} not found!")

      service = f'https://cmsdca.cern.ch/trk_loader/trker/{self.database}'

      cmd = f'python3 {self.dbloader} --login --url={service} {filename}'
      if isTest:  cmd = cmd + ' --test'
      
      exe = [ '/bin/bash','-c', cmd ]

      out  = ''
      attemps = 0

      while 'loaded' not in out and attemps < 5:
         p = subprocess.Popen(exe, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                              bufsize=0, close_fds=True, encoding='UTF-8')

         out,err = p.communicate()
         attemps+=1
         
         if self.verbose==True:
            print(f'\nUploading {filename},  attemp n. {attemps}')
            print(out)

            if 'loaded' in out.split('\n')[0]: 
               print(f'{filename} succesfully registered in the database.')
            else: 
               msg = f'{filename} not loaded,  retrying ...' if attemps < 5 else f'{filename} not loaded!'
               print(msg)         

      if 'loaded' not in out.split('\n')[0]:
         msg = f'Failed to upload {filename}\n\nCommand:\n{cmd}\n\nOutput:\n{out}'
         raise UploadFailure(self.__class__.__name__,msg) 
         
class DBaccess:
   """Service class for accessing the database content."""

   def __init__(self, database='trker_cmsr', verbose=False):
      """Constructor: it requires the database where to query the data back. By
         default it points to the production database. The development database
         can be accessed using 'trker_int2r'. The remote steps of the query are
         printed by setting verbose equal to True.
      """

      self.database = database
      self.verbose  = verbose


   def data_query(self,query):
      """Get the data from the database."""
      try:
         from rhapi import RhApi
      except:
         raise DBAPINotAvailable(self.__class__.__name__,f"RhApi not found!")
         #print( 'Cannot import rhapi: resthub package not in the path.' )
         #print( 'Download the resthub package and run bin/setup.sh' )
         #sys.exit(1)

      dburl = 'https://cmsdca.cern.ch/trk_rhapi'
      DBapi = RhApi(dburl, debug=self.verbose, sso='login')
      if query=='':  raise BadQuery(self.__class__.__name__, query)
      DBapi.clean(DBapi.qid( query ) )
      Qres  = DBapi.json2( query )
      if self.verbose:  print( Qres )
      return Qres['data']

   def component_id(self, id, idt='name_label'):
      """Returns the part_id of a component registered in the database.
         Returns None if the component is not registered."""
         
      query='select p.id from {}.parts p where p.{}=\'{}\''.\
             format(self.database,IdType[idt].name,id)
      #query='select p.id from {}.parts p where p.kind_of_part_id=\'1140\''.\
    #         format(self.database)
      if self.verbose:  print( query )
      data = self.data_query( query )
      try:
         return str(data[0]['id'])
      except:
         return None
      #if len( data )!=0: return 
      #return False


class UploaderContainer:
   """Class that holds many uploader all from the same class type."""

   def __init__(self, name, inserter=None):
      """Basic initializer."""
      self.name      = name
      self.inserter  = inserter
      self.uploaders = []

   def add(self,uploader):
      """Method to add an uploader in the container."""
      if len(self.uploaders)!=0:
         if type(self.uploaders[0])==type(uploader):
            already_in = False
            for up in self.uploaders:
               if up.name==uploader.name:  already_in=True
            if not already_in:
               self.uploaders.append(uploader)
      else: self.uploaders.append(uploader)

   def dump_xml_data(self, *args):
      """Writes the uoploaders content in a unique XML file to be uploaded in
         the database. It requires in input the arguments to be passed to each
         uploader classes for producing the XML file blocks."""
      root      = etree.Element("ROOT")
      if self.inserter is not None:
         root.set('operator_name',self.inserter)
         
      parts = etree.SubElement(root,"PARTS")

      Nup = len(self.uploaders)
      Wig = [f'Writing {self.name}.xml: ', Bar('=', '[', ']'), ' ', Percentage()]

      for i in progressbar(range(Nup), widgets=Wig, redirect_stdout=True):
         uploader = self.uploaders[i]
         print(f'{uploader.name} processed!')
         uploader.xml_builder(parts,*args)
         time.sleep(0.1)
      
      filename = f'{self.name}.xml'
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      return filename
   
   
   def dump_single_xml_data(self, *args):
      """Writes the uoploaders content in multiple XML files to be uploaded in
         the database. It requires in input the arguments to be passed to each
         uploader classes for producing the XML file blocks."""
      
      files = []
      Nup = len(self.uploaders)
      Wig = [f'Writing {self.name}.xml: ', Bar('=', '[', ']'), ' ', Percentage()]

      for i in progressbar(range(Nup), widgets=Wig, redirect_stdout=True):  
         root      = etree.Element("ROOT")
         if self.inserter is not None:
            root.set('operator_name',self.inserter)
         
         parts = etree.SubElement(root,"PARTS")
         uploader = self.uploaders[i]
         print(f'{uploader.name} processed!')
         uploader.xml_builder(parts,*args)
         time.sleep(0.1)
      
         filename = f'{uploader.name}.xml'
         with open(filename, "w") as f:
            f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
         files.append(filename)
         
      return files
   

   def dump_xml_zip(self):
      """Writes the uoploaders content in separate XML files which are zipped
         together for being uploaded in the database."""
      file_list = []
      for uploader in self.uploaders:
         file_list.append( '{}_data.xml'.format(uploader.name) )
         uploader.dump_xml_data()

      from zipfile import ZipFile
      zipfilename = '{}.zip'.format(self.name)
      with ZipFile(zipfilename, 'w') as (zip):
         for f in file_list: zip.write(f)
         for f in file_list: os.remove(f)
      return zipfilename
