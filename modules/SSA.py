#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 29-Nov-2022

# This module contains the classes that produce the XML file for uploading
# the SSA Wafer, Chip and their Condition data.


from AnsiColor    import Fore, Back, Style
from Exceptions   import MissingData, BadStripInconsistency, BadMeasurement,\
                         NotRecognized, MissingParameter, BadParameters
from BaseUploader import BaseUploader,ComponentType, ConditionData
from lxml         import etree
from time         import gmtime, strftime
from enum         import Enum
from copy         import deepcopy
from datetime     import date
from dateutil     import parser

import os,sys


   
    
class SSACICwafer(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the SSA Wafer
      component data read from the csv file."""
   db_check = True

   def __init__(self, configuration, data):
      """Constructor: it requires the dictionary with the default configuration 
         (configuration) and the dictionary with component data (data).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the type of part to be inserted in the database;
            unique_location: the location where the component is located;
         Optional parameters in configuration:
            version:         the version tag used to define the kind of part;
         Keys searched in data:
            lot_name:        the wafer production lot
            wafer:           the wafer id
            date:            the wafer production date
      """

      import copy
      self.type = ComponentType.SSAwafer
      cdict = copy.deepcopy(configuration)

      # Use TMSC as the default for Manufacturer and CERN for Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'TSMC'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'CERN'
      
      cdict['kind_of_part'] = 'SSA-CIC Wafer'
      cdict['attributes']   = [('Status','Good')]
      
      # gather the component data
      try:
         wafer_lot = data['lot_name']
      except:
         raise MissingData(self.__class__.__name__,'lot_name')
      try:
         wafer_number = data['wafer']
      except:
         raise MissingData(self.__class__.__name__,'wafer')
      try:
         d = parser.parse(data['date'])
         cdict['product_date'] = date.strftime(d,'%Y-%m-%d')
      except:
         raise MissingData(self.__class__.__name__,'date')

      # The class name is the name_label of the Wafer written in the txt file
      name = '{}_{}'.format(wafer_lot,wafer_number)


      BaseUploader.__init__(self, name, cdict)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                          'attributes']

   def name_label(self):
      """Returns the name label of the component."""
      return self.name
  
   def kind_of_part(self):
       """Returns the ckind of part of the component."""
       return self.retrieve('kind_of_part')

   def wafer_lot(self,name_label=None):
      """Return the batch id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[0]

   def wafer_number(self,name_label=None):
      """Return the wafer_id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[1]

   def match(self,name_label):
      """Return True if the inout name_label matches the class name."""
      if self.wafer_lot()    == self.wafer_lot(name_label) and\
         self.wafer_number() == self.wafer_number(name_label):
        return True
      return False

   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts)

      if filename == '':
         filename = '{}_wafer.xml'.format(self.name_label())

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      
      return filename



   def xml_builder(self, parts, *args):
      """Processes data."""
      registered = False
      if self.db_check:
         part_id = self.db.component_id(self.name_label(),'name_label')

      if part_id==None:
         # not registered
         self.build_parts_on_xml(parts,name_label=self.name_label())


class SSAchip(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the SSA chip
      component data."""
   db_check = False

   def __init__(self, configuration, data):
      """it requires the dictionary with the default configuration 
         (configuration) and the dictionary with component data (data).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the type of part to be inserted in the database;
            unique_location: the location where the component is located;
            version:         the version tag used to define the kind of part;
         Keys searched in data:
            lot_name:        the wafer production lot
            wafer:           the wafer id
            date:            the wafer production date
            row              the row position of the reticle
            col              the column position of the reticle
            pos              the chip position in the reticle
            reticle          the reticle id in the wafer
            efuse_id         the id efused in the chip at test time
            pass_fail        flag telling if chip has passed the test
            damaged          flag telling is chip has been damaged
            pos_x            the x position of the chip in the reticle
            pos_y            the y position of the chip in the reticle
            """

      import copy
      self.type = ComponentType.SSAchip
      cdict = copy.deepcopy(configuration)

      # Use TMSC as the default for Manufacturer and CERN for location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'TSMC'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'CERN'

      cdict['kind_of_part'] = 'SSA Chip'
      cdict['attributes']   = []

      # gather the component data
      try:
         wafer_lot = data['lot_name']
      except:
         raise MissingData(self.__class__.__name__,'lot_name')
      try:
         wafer_number = data['wafer']
      except:
         raise MissingData(self.__class__.__name__,'wafer')
      try:
         d = parser.parse(data['date'])
         cdict['product_date'] = date.strftime(d,'%Y-%m-%d')
      except:
         raise MissingData(self.__class__.__name__,'date')
      try:
         row = data['row']
         cdict['attributes'].append(('Reticle Row on Wafer',row))
      except:
         raise MissingData(self.__class__.__name__,'row')
      try:
         col = data['col']
         cdict['attributes'].append(('Reticle Column on Wafer',col))
      except:
         raise MissingData(self.__class__.__name__,'col')
      try:
         pos = data['pos']
         cdict['attributes'].append(('Die Posn on Reticle',pos))
      except:
         raise MissingData(self.__class__.__name__,'pos')
      try:
         cdict['attributes'].append(('Reticle Posn on Wafer',data['reticle']))
      except:
         raise MissingData(self.__class__.__name__,'reticle')
      try:
         cdict['serial_number'] = data['efuse_id']
      except:
         raise MissingData(self.__class__.__name__,'efuse_id')
      try:
         cdict['has_passed_test'] = data['pass_fail']
      except:
         raise MissingData(self.__class__.__name__,'pass_fail')
      try:
         cdict['is_damaged'] = data['damaged']
      except:
         raise MissingData(self.__class__.__name__,'damaged')
      try:
         cdict['pos_x'] = data['pos_x']
      except:
         raise MissingData(self.__class__.__name__,'pos_x')
      try:
         cdict['pos_y'] = data['pos_y']
      except:
         raise MissingData(self.__class__.__name__,'pos_y')

      
      # The class name is the name_label of the SSA chip
      try:
         name = f'{wafer_lot}_{wafer_number}_{int(row):X}{int(col):X}{int(pos):X}'
      except:
         from json import dumps
         raise BadParameters(self.__class__.__name__,dumps(data))

      BaseUploader.__init__(self, name, cdict)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                         'attributes','version','has_passed_test','is_damaged']



   def name_label(self):
      """Returns the name label of the component."""
      return self.name
   
   def kind_of_part(self):
      """Returns the kind of part of the component."""
      return self.retrieve('kind_of_part')

   def serial(self):
      """Returns the serial number of the component."""
      return self.retrieve('serial_number')

   def wafer_lot(self,name_label=None):
      """Return the wafer lot from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[0]

   def wafer_number(self,name_label=None):
      """Return the wafer_number from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[1]

   def chip_id(self,name_label=None):
      """Return the chip_id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[2]

   def match(self,name_label):
      """Return True if the inout name_label matches the class name."""
      if self.wafer_lot()    == self.wafer_lot(name_label) and\
         self.wafer_number() == self.wafer_number(name_label) and\
         self.chip_id()      == self.chip_id(name_label):
        return True
      return False

   def dump_xml_data(self,filename='',wafers=None):
      """Writes the sensor component in the XML file for the database upload.
         It requires the name of the XML file to be produced. A SSAwafer
         class can also be input: in this case a parent-child relationship
         with the Wafer components is set (the wafer components must have
         been already entered in the database).
      """
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts,wafers)

      if filename == '':
         filename = '%s_chips.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

      return filename
  

   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      wafers = args[0]

      children = parts
      if wafers is not None:
         for wafer in wafers:
            if wafer.match(self.name_label()):
               wafer_kop  = wafer.kind_of_part()
               wafer_part = etree.SubElement(parts, "PART", mode="auto")
               etree.SubElement(wafer_part,"KIND_OF_PART").text  = wafer_kop
               etree.SubElement(wafer_part,"NAME_LABEL").text = wafer.name_label()
               children = etree.SubElement(wafer_part,"CHILDREN")

      part_id = None
      if self.db_check:
         part_id = self.db.component_id(self.name_label(),'name_label')
            
      if part_id==None:
        # not registered
        passed  = 'T' if self.retrieve('has_passed_test')=='1' else 'F'
        damaged = 'T' if self.retrieve('is_damaged')=='1' else 'F'
        
        if passed=='T' and damaged=='F':
          self.configuration['attributes'].append(('Status','Good'))
        else:
          self.configuration['attributes'].append(('Status','Bad'))
          
        ex = [ ('HAS_PASSED_TEST', passed), ('IS_DAMAGED', damaged) ]
        if self.retrieve('pos_x'):
          ex.append( ( 'POS_X', self.retrieve('pos_x')) )
        if self.retrieve('pos_y'):
          ex.append( ( 'POS_Y', self.retrieve('pos_y')) )
        
        # Exit if the serial number is already in use
        if self.serial()!='':
          self.build_parts_on_xml(children,name_label=self.name_label(),serial=self.serial(),extended_data=ex)
        else:
          self.build_parts_on_xml(children,name_label=self.name_label(),extended_data=ex)
      else:
        print(f'Component {self.name_label()} already registered in the database, skipping it!')
